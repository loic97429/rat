# Client
A simple backdoor written in python that uses subprocess and requests module. 

The principle is simply to use POST requests to the attacker website to receive commands and send results. 

Be sure to launch with 'dev' arg while testing in local to not use the "inject into startup programs" part of the code and to redirect the shell to localhost.


## requirements
Prefer to use python3.7

```bash
> python -m pip install -r requirements.txt
```  

## camera interactions requirements

In `C:\Users\YOURUSERNAME\AppData\Local\Programs\Python\Python37\lib\site-packages\VideoCapture\__init__.py`, line 153 change `Image.fromstring(...)` to `Image.frombytes(...)`

In `Lib\site-packages\PIL\ImageFont.py` line 710 change `pass` to `return load_default()` as following : 
```python
def load_path(filename):
"Load a font file, searching along the Python path."
for dir in sys.path:
    if Image.isDirectory(dir):
        try:
            return load(os.path.join(dir, filename))
        except IOError:
            return load_default()  # this line
raise IOError("cannot find font file")
```

## Important variables
Go to client folder
create TOKENS.py and fill the variables (see section How to generate token):
  - TOKEN : same as TOKEN_client in adminpanel/TOKEN.py
  - url : url to admin panel website api (ex: 'http://localhost:5000/api/' if testing in local)
## compiling
  you need to install PyInstaller to be able to compile : 
  ```bash
  > py -m pip install PyInstaller
  ```

  Then simply use COMPILE_backdoor.bat


# Admin Panel
A website to receive requests from the victim.

Here are the main pages : 
  - Panel with list of hosts and their respective status UP or DOWN (LOGIN REQUIRED)
  - iplookup to see location & other interesting thing about an IP
  - interactive shell to an online host (LOGIN REQUIRED)
  - uploads to see all of the dumped files (LOGIN REQUIRED)
  - payloads (where you can place whatever you want that can be downloaded from anywhere)

Be sure to launch with 'dev' arg while testing in local to use the sqlite database instead of searching in environement variables for a postgres database url : 
```bash
> cd adminpanel
> py app.py dev
```

## Important variables
create 'adminpanel/TOKENS.py' in the SAME folder as app.py and fill the variables (see section How to generate token) :
  - TOKEN_server (see section How to generate token)
  - TOKEN_client
  - APP_SECRET_KEY
  - username
  - password

## Production with heroku
create an heroku account and download the heroku client, then do the following commands
```bash
> cd adminpanel
> heroku login
> heroku create
> heroku addons:create heroku-postgresql:hobby-dev
> git add .
> git commit -m "getting my site on production"
> git push heroku
```

# How to generate token
The following command in python interactive console gives you a randomized 12 bytes token you can copy paste in the variables that need one
```python
>>> import secrets
>>> secrets.token_hex(12)
```

## TODO
  - microphone
  - history IE
  - password IE
  - password Firefox
  - clipboard