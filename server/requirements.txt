flask
requests
asyncio
beautifulsoup4
flask-wtf
flask-sqlalchemy
flask-bcrypt
flask-login
pillow
flask-mail
flask-migrate
flask-script
gunicorn
psycopg2
ipwhois
